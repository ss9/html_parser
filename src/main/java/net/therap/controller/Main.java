package net.therap.controller;

import net.therap.domain.HTMLParser;
import net.therap.helper.Helper;

/**
 * @author shadman
 * @since 1/3/18
 */
public class Main {
    public static void main(String[] args) {

        if (args.length == 2) {
            HTMLParser htmlParser = new HTMLParser(args[0]);
            htmlParser.parseAndWrite(args[1]);
        }

    }
}
