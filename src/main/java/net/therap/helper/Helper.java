package net.therap.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shadman
 * @since 1/3/18
 */
public class Helper {

    public static final int INDENTATION_SPACES = 4;
    public static final String ADDED_MISSING_CLOSING_TAG = " <!--added missing closing tag -->";

    public static final String REGEX_CONSECUTIVE_WHITESPACES = "( )+";
    public static final String REGEX_HTML_COMMENT = "<!--(.*?)-->";
    public static final String REGEX_STYLE = "(?s)<style(.*?)</style>";
    public static final String REGEX_SCRIPT = "(?s)<script(.*?)</script>";
    public static final String REGEX_OPENING_TAG_FOLLOWING_SPACE = " <";
    public static final String SPACE_FOLLOWING_CLOSING_TAG = "> ";

    public static final List<String> EXCLUDES;

    public static final List<String> NON_CLOSING_TAGS;

    static {
        EXCLUDES = new ArrayList<>();
        EXCLUDES.add("style");
        EXCLUDES.add("script");

        NON_CLOSING_TAGS = new ArrayList<>();
        NON_CLOSING_TAGS.add("meta");
        NON_CLOSING_TAGS.add("link");
        NON_CLOSING_TAGS.add("input");
        NON_CLOSING_TAGS.add("hr");
        NON_CLOSING_TAGS.add("br");
        NON_CLOSING_TAGS.add("img");
        NON_CLOSING_TAGS.add("!doctype");
    }
}
