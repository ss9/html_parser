package net.therap.domain;

import net.therap.helper.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Stack;

/**
 * @author shadman
 * @since 1/3/18
 */
public class HTMLParser {

    private String fileName;
    Stack<String> tags;
    private int indentBy;
    String fileToString;
    String formattedString;

    public HTMLParser(String fileName) {
        this.fileName = fileName;
        this.tags = new Stack<>();
        this.indentBy = 0;
        this.fileToString = "";
        this.formattedString = "";
    }

    public void parseAndWrite(String fileName) {
        this.readFileIntoString();
        this.removeExtraWhiteSpaces();
        this.removeComments();
        this.addMissingTags();
        this.removeStylesAndScripts();
        this.writeResult(fileName);
    }

    public void readFileIntoString() {
        File file;
        FileReader fileReader;
        BufferedReader bufferedReader;
        String line;

        try {
            file = new File(this.fileName);
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                this.fileToString += (line);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void removeExtraWhiteSpaces() {
        this.fileToString = this.fileToString.trim().replaceAll(Helper.REGEX_CONSECUTIVE_WHITESPACES, " ");
        this.fileToString = this.fileToString.replaceAll(Helper.REGEX_OPENING_TAG_FOLLOWING_SPACE, "<");
        this.fileToString = this.fileToString.replaceAll(Helper.SPACE_FOLLOWING_CLOSING_TAG, ">");
    }

    public void removeComments() {
        this.fileToString = this.fileToString.replaceAll(Helper.REGEX_HTML_COMMENT, "");
    }

    public void addMissingTags() {
        int currentIndex = 0;
        boolean addNewLine = false;
        boolean indentContent = false;
        while (currentIndex < this.fileToString.length()) {

            if (fileToString.charAt(currentIndex) == '<') {

                boolean closingTag = false;
                boolean selfClosingTag = false;

                currentIndex++;

                if (addNewLine) {
                    this.formattedString += "\n";
                    addNewLine = false;
                }

                if (currentIndex < this.fileToString.length() && fileToString.charAt(currentIndex) == '/') {
                    closingTag = true;
                    currentIndex++;
                }

                String currentTag = "";

                while (currentIndex < this.fileToString.length() && this.fileToString.charAt(currentIndex) != '>') {
                    currentTag += this.fileToString.charAt(currentIndex);
                    currentIndex++;
                }

                if (closingTag) {

                    while (!tags.empty() && !currentTag.equals(tags.peek())) {
                        this.indentBy -= Helper.INDENTATION_SPACES;
                        this.formattedString += addSpaces(this.indentBy);
                        this.formattedString += ("</" + this.tags.peek() + ">");

                        if (!Helper.EXCLUDES.contains(this.tags.peek())) {
                            this.formattedString += Helper.ADDED_MISSING_CLOSING_TAG;
                        }

                        this.formattedString += "\n";
                        this.tags.pop();
                    }

                    if (!tags.empty()) {
                        this.indentBy -= Helper.INDENTATION_SPACES;
                        this.formattedString += addSpaces(this.indentBy);
                        this.formattedString += ("</" + this.tags.pop() + ">");
                        this.formattedString += "\n";
                    }

                } else {
                    String[] splitTag = currentTag.split(" ");
                    String lastSplit = splitTag[splitTag.length - 1];

                    if (lastSplit.charAt(lastSplit.length() - 1) != '/') {

                        if (!Helper.NON_CLOSING_TAGS.contains(splitTag[0].toLowerCase())) {
                            this.tags.push(splitTag[0]);
                        } else {
                            selfClosingTag = true;
                        }

                    } else {
                        selfClosingTag = true;
                    }

                    boolean startNewLine = false;

                    this.formattedString += addSpaces(this.indentBy);
                    this.formattedString += ("<" + splitTag[0]);

                    if (splitTag.length > 1) {
                        this.formattedString += (" " + splitTag[1]);
                        startNewLine = splitTag[1].charAt(splitTag[1].length() - 1) == '"';
                    }

                    for (int i = 2; i < splitTag.length; i++) {

                        if (startNewLine) {
                            this.formattedString += ("\n" + addSpaces(this.indentBy + splitTag[0].length() + 2));
                        } else {
                            this.formattedString += " ";
                        }

                        this.formattedString += splitTag[i];

                        startNewLine = splitTag[i].charAt(splitTag[i].length() - 1) == '"';
                    }

                    this.formattedString += ">";
                    this.formattedString += "\n";

                    if (!selfClosingTag) {
                        this.indentBy += Helper.INDENTATION_SPACES;
                    }
                }

                indentContent = true;
            } else {

                if (indentContent) {
                    this.formattedString += addSpaces(this.indentBy);
                    indentContent = false;
                }

                this.formattedString += this.fileToString.charAt(currentIndex);
                addNewLine = true;
            }

            currentIndex++;
        }

        while (!tags.empty()) {
            this.indentBy -= Helper.INDENTATION_SPACES;
            this.formattedString += "\n";
            this.formattedString += addSpaces(this.indentBy);
            this.formattedString += ("</" + this.tags.peek() + ">");

            if (!Helper.EXCLUDES.contains(this.tags.peek())) {
                this.formattedString += Helper.ADDED_MISSING_CLOSING_TAG;
            }

            this.formattedString += "\n";
            this.tags.pop();
        }
    }

    public void removeStylesAndScripts() {
        this.formattedString = this.formattedString.replaceAll(Helper.REGEX_STYLE, "");
        this.formattedString = this.formattedString.replaceAll(Helper.REGEX_SCRIPT, "");
    }

    public String addSpaces(int indentBy) {
        String spaces = "";

        for (int i = 0; i < indentBy; i++) {
            spaces += " ";
        }

        return spaces;
    }

    public void writeResult(String fileName) {
        this.formattedString = this.formattedString.replaceAll("( )*(\n)+", "\n");
        this.formattedString = this.formattedString.replaceAll("(\n)(\n)+", "\n");
        if(this.formattedString.startsWith("\n")) {
            this.formattedString = this.formattedString.substring(1);
        }

        try {
            Path path = Paths.get(fileName);
            byte[] strToBytes = this.formattedString.getBytes();
            Files.write(path, strToBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
